"""
Module for Proxy requests
"""
import copy
import random
from urllib.parse import urlparse
import requests
from .proxies import proxies

class ProxyRequests:
    """
    Class of free web proxies that could be used
    while scraping.
    :param server: name of the proxy to use default ('random')
             
    :param default_headers: (optional) a spesific header 
                            to use when making the requests.
    :return: :class:`Response <Response>` object
    :rtype: requests.Response
    """
    def __init__(self, server='random',
                 default_headers=None) -> None:
        self.default_headers = default_headers
        self.proxies = proxies
        self.proxy_copy = None
        self.server = server
        self.proxy= None
    def get_server(self):
        """
        Method to get the information of the proxy server
        """
        if self.server == 'random':
            self.proxy= random.choice(list(self.proxies))
        elif self.server == 'continuous':
            if not self.proxy_copy:
                self.proxy_copy = copy.deepcopy(self.proxies)
            choice = random.choice(list(self.proxy_copy))
            self.proxy= choice
            self.proxy_copy.pop(choice)
        elif self.server in self.proxies:
            self.proxy= self.server
        elif self.server == 'manual':
            pass
        else:
            raise TypeError("Error! server must be provided")
    def url_parse(self, url):
        """
        method to adjust the parse url
        """
        parsed_url = urlparse(url)
        headers = {'authority': parsed_url.hostname,
                    'method': 'GET',
                    'path': url.split(parsed_url.hostname)[1],
                    'scheme': parsed_url.scheme,
                    'accept': '*/*',
                    'accept-encoding': 'gzip, deflate, br',
                    'accept-language': 'fr-FR,fr;q=0.9,en-US;q=0.8,en;q=0.7,ar;q=0.6',
                    'sec-ch-ua': '"Not?A_Brand";v="8", "Chromium";v="108", "Google Chrome";v="108"',
                    'sec-ch-ua-mobile': '?0',
                    'sec-ch-ua-platform': "Windows",
                    'sec-fetch-dest': 'empty',
                    'sec-fetch-mode': 'cors',
                    'sec-fetch-site': 'same-origin',
                    'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) '\
                    'AppleWebKit/537.36 (KHTML, like Gecko) Chrome/108.0.0.0 Safari/537.36'}
        return headers
    def fill_proxy_params(self, url):
        """
        Method to fill in the necessary parameters to use the proxy
        Parameters may changes from a proxy to another
        """
        proxy_name = self.proxy
        proxy_url = self.proxies[proxy_name]['url']
        params = self.proxies[proxy_name]['params']
        proxy_data = copy.deepcopy(params)
        url_key = proxy_data['url_key']
        proxy_data.pop('url_key',None)
        proxy_data[url_key] = url
        return proxy_url, proxy_data
    def get(self, url, headers=None):
        """
        'GET' Method Same as get from requests just using a web proxy
        """
        self.get_server()
        proxy_url, data = self.fill_proxy_params(url)
        if self.default_headers:
            headers = self.url_parse(url)
        try:
            response = requests.post(proxy_url,
                                data=data,
                                headers=headers,
                                timeout=10)
        except:# pylint: disable=bare-except
            # to update [retry]
            print(proxy_url, 'Faild')
            response = None
        return response
    def post(self, url, params):
        """
        Method post behind the proxy requests
        params:
           url (str): the url that you want to request
        returns:
           response
        """
        self.get_server()
        proxy_url, data = self.fill_proxy_params(url)
        response = requests.post(proxy_url,data=data,
                            params=params,
                            timeout=10)
        return response
if __name__ == '__main__':
    pass
