## free proxy request

This Project is to create a tool for changing ip addresses while using requests lib.

## Installation : (Install from source)
```
$ git clone https://gitlab.com/lackti/free-proxy-request.git
$ cd free-proxy-request
$ python -m build
```

let's try just to call a website which simply return the ip address in JSON format  
you can try it out https://api.ipify.org/?format=json   
in the example below we will use the requests library and our custom ProxyRequests class   
```
from proxy_requests.proxy_requests import ProxyRequests
import requests

url = 'https://api.ipify.org/?format=json'
proxy_requests = ProxyRequests()

response_proxy = proxy_requests.get(url)
response = requests.get(url)

my_ip = response.json()['ip']
proxy_ip = response_proxy.json()['ip']

print('My ip address {}'.format(my_ip))
print('My proxy ip address {}'.format(proxy_ip))
```

